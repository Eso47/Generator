const $ = require('jquery');
const Columnist = require('avris-columnist');

const grid = document.querySelector('.grid');
let columnist = null;

if (grid) {
    columnist = new Columnist(grid);
    columnist.start();
}

const $filterBtns = $('[data-filter-target]');

$filterBtns.click(function() {
    const $btn = $(this);
    const $target = $($btn.data('filter-target'));
    const value = $btn.data('filter-value');
    const on = value && $btn.hasClass('active');
    const expected = on ? '' : value;

    $filterBtns.removeClass('active');
    $filterBtns.filter(`[data-filter-value="${value}"]`).toggleClass('active', !on);

    window.history.pushState(null, null, on ? '/' : $btn.attr('href'));

    $target.find('[data-filter-value]').each(function () {
        const $el = $(this);
        const stays = expected === '' || $el.data('filter-value') === expected;
        $el.toggleClass('grid-item-hidden', !stays)
    });

    if (columnist) {
        columnist.destroy();
        columnist.start();
    }

    $btn.blur();

    return false;
});

$('[data-filter-init]').each(function() {
    const $filter = $(this);
    const init = $filter.data('filter-init');
    if (init) {
        $filter.find(`[data-filter-value="${init}"]`).click();
    }
});
