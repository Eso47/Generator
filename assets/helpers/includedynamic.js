const pug = require('pug');
const path = require('path');
const translator = require('./translator');

module.exports = (filename) => {
    try {
        return pug.renderFile(path.join(__dirname, '..', '..', 'views', filename), {
            t: translator,
        });
    } catch (e) {
        if (e.message.startsWith('ENOENT: no such file or directory')) {
            return '';
        }
        throw e;
    }
};
