module.exports.randomElement = (list) => list[Math.floor(Math.random()*list.length)];

module.exports.randomise = (string, len) => {
    let out = '';
    for (let i = 0; i < len; i++) {
        out += string.charAt(Math.floor(Math.random() * string.length));
    }
    return out;
};

module.exports.letterToNumber = (num) => {
    if (isNaN(parseInt(num))) {
        return num.charCodeAt(0) - 55;
    }
    return parseInt(num);
};

module.exports.bigModulo = (divident, divisor) => {
    const partLength = 10;
    while (divident.length > partLength) {
        const part = divident.substring(0, partLength);
        divident = (part % divisor) +  divident.substring(partLength);
    }
    return divident % divisor;
};

module.exports.arrayTimesArray = (arr1, arr2) => {
    return Array.from(arr1).map((el, i) => module.exports.letterToNumber(arr2[i]) * module.exports.letterToNumber(el));
};

module.exports.sum = (arr) => {
    return Array.from(arr).reduce((t, s) => t + parseInt(s));
};

module.exports.crossSum = (arr) => {
    return Array.from(arr).join('').split('').reduce((t, s) => parseInt(t) + parseInt(s));
};

module.exports.unique = (arr) => {
    return Array.from(arr).filter((value, index, self) => self.indexOf(value) === index);
};

module.exports.randRange = (start, end) => Math.floor(Math.random() * (end - start)) + start;

module.exports.diffYears = (date1, date2) => {
    const diffMs = date1.getTime() - date2.getTime();
    const diffDate = new Date(diffMs);

    return diffDate.getUTCFullYear() - 1970;
};

module.exports.sumDigits = (digits) => {
    let sum = 0;
    for (let digit of '' + digits) {
        sum += parseInt(digit);
    }
    return sum;
};
