const Generator = require('../../abstractGenerator');
const {randomise, unique, randomElement, sumDigits, randRange, arrayTimesArray, sum, crossSum} = require('../../helpers');
const {digits, upper} = require('../../sets');
const handleParams = require('../../handleParams');

// specs: https://de.wikipedia.org/wiki/Versicherungsnummer

module.exports = class RVNR extends Generator {

    static get weights() {
        return [2, 1, 2, 5, 7, 1, 2, 1, 2, 1, 2, 1];
    }

    static get areas() {
        return {
            '02': 'Deutsche Rentenversicherung Nord (Mecklenburg-Vorpommern)',
            '03': 'Deutsche Rentenversicherung Mitteldeutschland (Thüringen)',
            '04': 'Deutsche Rentenversicherung Berlin-Brandenburg (Brandenburg)',
            '08': 'Deutsche Rentenversicherung Mitteldeutschland (Sachsen-Anhalt)',
            '09': 'Deutsche Rentenversicherung Mitteldeutschland (Sachsen)',
            '10': 'Deutsche Rentenversicherung Braunschweig-Hannover (Hannover)',
            '11': 'Deutsche Rentenversicherung Westfalen',
            '12': 'Deutsche Rentenversicherung Hessen',
            '13': 'Deutsche Rentenversicherung Rheinland (Rheinprovinz)',
            '14': 'Deutsche Rentenversicherung Bayern Süd (Oberbayern)',
            '15': 'Deutsche Rentenversicherung Bayern Süd (Niederbayern-Oberpfalz)',
            '16': 'Deutsche Rentenversicherung Rheinland-Pfalz',
            '17': 'Deutsche Rentenversicherung Saarland',
            '18': 'Deutsche Rentenversicherung Nordbayern (Ober- und Mittelfranken)',
            '19': 'Deutsche Rentenversicherung Nord (Hamburg)',
            '20': 'Deutsche Rentenversicherung Nordbayern (Unterfranken)',
            '21': 'Deutsche Rentenversicherung Schwaben',
            '23': 'Deutsche Rentenversicherung Baden-Württemberg (Württemberg)',
            '24': 'Deutsche Rentenversicherung Baden-Württemberg (Baden)',
            '25': 'Deutsche Rentenversicherung Berlin-Brandenburg (Berlin)',
            '26': 'Deutsche Rentenversicherung Nord (Schleswig-Holstein)',
            '28': 'Deutsche Rentenversicherung Oldenburg-Bremen',
            '29': 'Deutsche Rentenversicherung Braunschweig-Hannover (Braunschweig)',
            '38': 'Deutsche Rentenversicherung Knappschaft-Bahn-See (Wirtschaftsbereich Bahn)',
            '39': 'Deutsche Rentenversicherung Knappschaft-Bahn-See (Wirtschaftsbereich Seefahrt)',
            '40': 'Zentrale Zulagenstelle für Altersvermögen (Zulagenummer nach § 90 I 2 EStG)',
            '42': 'Deutsche Rentenversicherung Bund Nord (Mecklenburg-Vorpommern)',
            '43': 'Deutsche Rentenversicherung Bund Mitteldeutschland (Thüringen)',
            '44': 'Deutsche Rentenversicherung Bund Berlin-Brandenburg (Brandenburg)',
            '48': 'Deutsche Rentenversicherung Bund Mitteldeutschland (Sachsen-Anhalt)',
            '49': 'Deutsche Rentenversicherung Bund Mitteldeutschland (Sachsen)',
            '50': 'Deutsche Rentenversicherung Bund Braunschweig-Hannover (Hannover)',
            '51': 'Deutsche Rentenversicherung Bund Westfalen',
            '52': 'Deutsche Rentenversicherung Bund Hessen',
            '53': 'Deutsche Rentenversicherung Bund Rheinland (Rheinprovinz)',
            '54': 'Deutsche Rentenversicherung Bund Bayern Süd (Oberbayern)',
            '55': 'Deutsche Rentenversicherung Bund Bayern Süd (Niederbayern-Oberpfalz)',
            '56': 'Deutsche Rentenversicherung Bund Rheinland-Pfalz',
            '57': 'Deutsche Rentenversicherung Bund Saarland',
            '58': 'Deutsche Rentenversicherung Bund Nordbayern (Ober- und Mittelfranken)',
            '59': 'Deutsche Rentenversicherung Bund Nord (Hamburg)',
            '60': 'Deutsche Rentenversicherung Bund Nordbayern (Unterfranken)',
            '61': 'Deutsche Rentenversicherung Bund Schwaben',
            '63': 'Deutsche Rentenversicherung Bund Baden-Württemberg (Württemberg)',
            '64': 'Deutsche Rentenversicherung Bund Baden-Württemberg (Baden)',
            '65': 'Deutsche Rentenversicherung Bund Berlin-Brandenburg (Berlin)',
            '66': 'Deutsche Rentenversicherung Bund Nord (Schleswig-Holstein)',
            '68': 'Deutsche Rentenversicherung Bund Oldenburg-Bremen',
            '69': 'Deutsche Rentenversicherung Bund Braunschweig-Hannover (Braunschweig)',
            '78': 'Deutsche Rentenversicherung Bund Knappschaft-Bahn-See (Wirtschaftsbereich Bahn)',
            '79': 'Deutsche Rentenversicherung Bund Knappschaft-Bahn-See (Wirtschaftsbereich Seefahrt)',
            '80': 'Deutsche Rentenversicherung Knappschaft-Bahn-See (Berlin, Bremen, Hamburg, Niedersachsen, Westfalen und Schleswig-Holstein)',
            '81': 'Deutsche Rentenversicherung Knappschaft-Bahn-See (Hessen und Rheinprovinz)',
            '82': 'Deutsche Rentenversicherung Knappschaft-Bahn-See (Baden-Württemberg, Bayern, Rheinland-Pfalz und Saarland)',
            '89': 'Deutsche Rentenversicherung Knappschaft-Bahn-See (Brandenburg, Mecklenburg-Vorpommern, Sachsen-Anhalt, Sachsen und Thüringen)',
        };
    }

    params() {
        return {
            area: {
                type: 'string',
                values: Object.keys(RVNR.areas),
                default: null,
            },
            birthdate: {
                type: 'string',
                pattern: '^(18|19|20|21|22)[0-9][0-9](-[0-9][0-9])?(-[0-9][0-9])?$',
            },
            gender: {
                type: 'string',
                values: ['m', 'f', 'x', 'mf', 'mx', 'fx', 'mfx'],
                default: 'mfx',
            },
            surname: {type: 'string'},
        };
    }

    generate(params = {}) {
        const area = params.area || randomElement(Object.keys(RVNR.areas));

        let [y, m, d] = this._parseDateRequirement(params.birthdate);
        if (!y) {
            y = randRange(1900, 2020);
        }
        if (!m) {
            m = randRange(1, 13);
        }
        if (!d) {
            d = randRange(1, 29);
        }

        const initial = (params.surname || '').toUpperCase().substring(0, 1).normalize("NFD").replace(/[\u0300-\u036f]/g, "") || randomElement(upper);

        let genderRangeMin = 9999;
        let genderRangeMax = -1;
        if (params.gender.includes('m')) {
            genderRangeMin = 0;
            genderRangeMax = 50;
        }
        if (params.gender.includes('f') || params.gender.includes('x')) {
            genderRangeMin = Math.min(genderRangeMin, 50);
            genderRangeMax = Math.max(genderRangeMax, 100);
        }
        const serial = randRange(genderRangeMin, genderRangeMax);

        const nr = area
            + d.toString().padStart(2, '0')
            + m.toString().padStart(2, '0')
            + y.toString().slice(2, 4)
            + this._initialToNumber(initial)
            + serial.toString().padStart(2, '0');

        const checksum = crossSum(arrayTimesArray(RVNR.weights, nr)) % 10;

        return area
            + d.toString().padStart(2, '0')
            + m.toString().padStart(2, '0')
            + y.toString().slice(2, 4)
            + initial
            + serial.toString().padStart(2, '0')
            + checksum;
    }

    validate(value) {
        value = value.trim();

        if (!value.match(/^\d{8}[A-Z]\d{3}$/g)) {
            throw new Error('format')
        }

        const area = value.substring(0, 2);

        if (!RVNR.areas.hasOwnProperty(area)) {
            throw Error('area');
        }

        let [d, m, y] = [parseInt(value.slice(2, 4)), parseInt(value.slice(4, 6)), parseInt(value.slice(6, 8))];

        if ((m > 12) || (d > 31) || (m === 0) || (d === 0)) {
            throw new Error('birthdate')
        }

        const birthdate = new Date(`${1900 + y}-${m.toString().padStart(2, '0')}-${d.toString().padStart(2, '0')}T00:00:00Z`);

        const nr = value.substring(0, 8)
            + this._initialToNumber(value.substring(8, 9))
            + value.substring(9, 11);

        const checksum = crossSum(arrayTimesArray(RVNR.weights, nr)) % 10;

        if (checksum !== parseInt(value.substring(11, 12))) {
            throw new Error('checksum')
        }

        return {
            area: RVNR.areas[area],
            birthdate: birthdate,
            surname: value.substring(8, 9),
            gender: parseInt(value.substring(9, 11)) < 50 ? 'm' : 'fx',
        };
    }

    _parseDateRequirement(date) {
        if (date === '') {
            return [undefined, undefined, undefined];
        }

        if (date.match(/^\d{4}$/g)) {
            return [parseInt(date), undefined, undefined];
        }

        if (date.match(/^\d{4}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), undefined];
        }

        if (date.match(/^\d{4}-\d{2}-\d{2}$/g)) {
            return [parseInt(date.slice(0, 4)), parseInt(date.slice(5, 7)), parseInt(date.slice(8, 10))];
        }

        throw new Error('Invalid date requirement');
    }

    _initialToNumber(initial) {
        return (initial.charCodeAt(0) - 64).toString().padStart(2, '0');
    }
};
