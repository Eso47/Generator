const Generator = require('../../abstractGenerator');
const {randomise, randomElement, arrayTimesArray, sum} = require('../../helpers');
const {digits} = require('../../sets');
const handleParams = require('../../handleParams');

module.exports = class NINO extends Generator {
    static get prefixFirst() {
        return 'ABCEGHJKLMNOPRSTWXYZ'.split('');
    }

    static get prefixSecond() {
        return 'ABCEGHJKLMNPRSTWXYZ'.split('');
    }

    static get prefixNotAllocated() {
        return ['ZZ', 'NT', 'TN', 'KN', 'BG', 'NK', 'GB'];
    }

    static get suffixPreferred() {
        return 'ABCD'.split('');
    }

    generate(params = {}) {
        return this._generatePrefix() + randomise(digits, 6) + randomElement(NINO.suffixPreferred);
    }

    _generatePrefix() {
        const prefix = randomElement(NINO.prefixFirst) + randomElement(NINO.prefixSecond);

        if (NINO.prefixNotAllocated.indexOf(prefix) > -1) {
            return this._generatePrefix();
        }

        return prefix;
    }

    validate(value) {
        value = value.replace(/[- \.]/g,'');
        if (!value.match(/^[A-Z][A-Z]\d{6}[A-Z]$/g)) {
            throw new Error('format')
        }

        if (NINO.prefixFirst.indexOf(value[0]) === -1
            || NINO.prefixSecond.indexOf(value[1]) === -1
            || NINO.prefixNotAllocated.indexOf(value.slice(0, 2)) > -1) {
            throw new Error('prefix')
        }

        return true;
    }
};
