const Generator = require('../../abstractGenerator');
const {randomise, arrayTimesArray, sum} = require('../../helpers');
const {upper, digits} = require('../../sets');

module.exports = class ID extends Generator {
    static get weights() {
        return [7, 3, 1, 0, 7, 3, 1, 7, 3];
    }

    generate(params = {}) {
        const part1 = `A${randomise(upper, 2)}`;
        const part2 = randomise(digits, 5);
        const checksum = sum(arrayTimesArray(ID.weights, part1 + '0' + part2));

        return part1 + (checksum % 10) + part2;
    }

    validate(value) {
        if (!value.match(/^[A-Z]{3}\d{6}$/g)) {
            throw new Error('format')
        }

        if ((sum(arrayTimesArray(ID.weights, value)) % 10) !== parseInt(value[3])) {
            throw new Error('checksum')
        }

        return true;
    }
};
